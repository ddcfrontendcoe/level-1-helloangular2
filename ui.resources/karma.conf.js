//This liberation from Webpack is brought to you by https://github.com/monounity/karma-typescript/tree/master/examples/angular2
//This is the main web site that outlined what's in this file - https://www.npmjs.com/package/karma-typescript
// Configuration for karma, which is what your unit testing is being done with.
module.exports = function(config) {
    config.set({

        // All the frameworks you want to use within karma
        frameworks: ["jasmine", "karma-typescript"],

        // Files to be loaded into the browser
        files: [
            { pattern: "jsonConfigs.mock.js"},
            { pattern: "base.spec.ts" },
            { pattern: "src/app/**/*.+(ts|html)" }
        ],

        // Used at run time to run something specific on each file.
        // WARNING: If the preprocessor alters your file in any way it may cause other failures
        preprocessors: {
            "**/!(*spec).ts": ["karma-typescript"],
            "**/*.spec.ts": ["karma-typescript"]
        },

        // Option from the npm package "karma-typescript"
        // Can visit the npm package page for information on each of these options
        karmaTypescriptConfig: {
            bundlerOptions: {
                entrypoints: /\.spec\.ts$/,
                transforms: [
                    require("karma-typescript-angular2-transform"),
                    require("karma-typescript-es6-transform")()
                ]
            },
            compilerOptions: {
                "allowJs": true,
                lib: ["ES2015", "DOM"]
            },
            reports: {
                "cobertura": {
                    "directory": "target/test_coverage",
                    "subdirectory" : "cobertura",
                    "filename": "cobertura.xml"
                },
                "html": "target/test_coverage/html",
                "text-summary": ""
            }
        },

        // A list of reporters to use
        // progress = shows number of tests executed
        // To find out about karma-typescript, and other reporter packages visit npm and search
        // "karma-reporter"
        reporters: ["progress", "karma-typescript"],

        // When karma starts up these browsers are also started, when karma is shutdown
        browsers: ["PhantomJS"],

        // list of karma plugins
        plugins : [
            "karma-typescript",
            'karma-jasmine',
            'karma-jasmine-html-reporter',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher'
        ]
    });
};
