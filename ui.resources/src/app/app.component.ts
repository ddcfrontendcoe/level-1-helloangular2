import {Component, OnInit} from '@angular/core';
import {templates} from "./services/template.service";



// Component templates are stored inside of script tags in ui.resources/src/index.html
@Component( {
    selector: 'root-comp',
    template: templates.GetTemplate('rootcomp.html')
} )
// This is the root component. This is the first component that loads.
export class AppComponent implements OnInit {

    constructor() {
    }

    // ngOnInit is required when your component implements the lifecycle hook OnInit.
    // It is called after Angular first displays data-bound properties and after any input properties are set.
    ngOnInit() {
    }
}
