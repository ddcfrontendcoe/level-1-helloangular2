export var ExampleDataMock = {
    "exampleData": [
        {
            "AccountId": "account1",
            "AccountInfo": [
                {
                    "accountStatus": "Active"
                }
            ]
        },
        {
            "AccountId": "account2",
            "AccountInfo": [
                {
                    "accountStatus": "Inactive"
                }
            ]
        },
        {
            "AccountId": "account3",
            "AccountInfo": [
                {
                    "accountStatus": "Suspended"
                }
            ]
        }
    ]
};