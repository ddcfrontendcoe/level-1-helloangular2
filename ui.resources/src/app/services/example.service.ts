import {ApiService} from "./api.service";
import {Injectable} from "@angular/core";
import {JSONConfigsService} from "./jsonConfigs.service";
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class ExampleService extends ApiService {
    constructor(http: Http, jsonConfigSvc: JSONConfigsService) {
        super(http, jsonConfigSvc);
    }

    getExampleData$(): Observable<any>{
        return super.get(this.jsonConfigSvc.getExampleDataUri(), null, true, false)
            .map((res: any) => res);
    }

}