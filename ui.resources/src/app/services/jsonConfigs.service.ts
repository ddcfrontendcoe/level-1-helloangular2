// This is a service file use to pull information from the jsonConfigs.mock.js
// This is just an example of one way a developer can manage configurations in an enterprise application.

import { Injectable } from '@angular/core';

declare var jsonConfigs: any;

@Injectable()
export class JSONConfigsService {

    private jCon: any;

    constructor() {
        this.jCon = jsonConfigs;
    }

    public isMocked() {
        return this.jCon.feature.isMocked;
    }

    public apiHeaders(): any {
        return this.jCon.feature.apiHeaders;
    }

    public apiUrl(): string {
        return this.jCon.global.apiUrl;
    }

    public getExampleDataUri(): string{
        return this.jCon.global.exampleUrl;
    }

}