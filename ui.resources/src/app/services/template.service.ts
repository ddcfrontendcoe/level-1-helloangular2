// This file is what allows the developer to write their component templates directly inside of index.html
// It parses through the index.html to find the desired script tag and pulls that for the component to use.

class TemplateService {

    templates: any = [];

    constructor () {
        this.parse(document.getElementById('_templates'));
    }

    parse = (dom: any) => {
        try {
            for(let node of dom.childNodes) {
                if ( node.nodeName === 'SCRIPT' ) {
                    this.templates[ node.id ] = node.innerHTML;
                }
            }
        } catch ( e ) {

        }
    }

    public GetTemplate(identifier: string)
    {
        if (this.templates[identifier])
            return this.templates[identifier];
        else
            return "";
    }

}
export var templates = new TemplateService();