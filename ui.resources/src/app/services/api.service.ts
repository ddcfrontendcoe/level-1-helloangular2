import { Injectable, OnInit } from '@angular/core';

import 'rxjs/add/operator/publishLast';
import 'rxjs/add/operator/map';

import * as extend from '../../../node_modules/lodash/extend.js';

import { Http, URLSearchParams} from "@angular/http";
import { Observable } from "rxjs";
import { JSONConfigsService } from "./jsonConfigs.service";

// This file is used as a way of making service calls simple.
// What it does:
// 1) Set default headers, and allows the developer to add in any additional headers
// 2) Automatically sets a portion of the api url, and the developer passes in the portion that is missing.
// 3) Returns this request as an observable
// This file can also be altered to allow the user to see any other additional options they may want to send with
// the http.get request. Essentially it just creates a friendly way for developers to interact with the http.get functionality.

@Injectable()
export class ApiService implements OnInit {

    protected http: Http;
    protected jsonConfigSvc: JSONConfigsService;
    private queryParams : URLSearchParams;

    static headers: any = {
        'X-useragenttype': navigator.userAgent,
        'X-osversion': navigator.appVersion
    };

    constructor( http: Http, jsonConfigSvc: JSONConfigsService ) {
        this.http = http;
        this.jsonConfigSvc = jsonConfigSvc;
    }

    ngOnInit() {
    }

    get( resource: string, additionalHeaders: any = null, useRoot: boolean = true, nodeHeaders: boolean = false) {
        var root = this.jsonConfigSvc.apiUrl();
			
        let headers: any;
        headers = extend(headers, ApiService.headers);

        headers = extend(headers, this.jsonConfigSvc.apiHeaders());

        if (additionalHeaders != null)
            headers = extend(headers, additionalHeaders);

        let url;

        if (useRoot)
            url = root + resource;
        else
            url = resource;

        return this.http.get( url, { headers: headers,search: this.queryParams, withCredentials: true } )
            .map( this._map, this )
            .publishLast()
            .refCount()
    }

    setSearchParams(params: URLSearchParams){
        this.queryParams = params;
    }

    _map( value: any, index: any ) {
        return value.json();
    }

    _error(err: any, caught: Observable<any>): any {
        return caught;
    }
}