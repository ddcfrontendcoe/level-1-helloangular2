import {async, inject, TestBed} from "@angular/core/testing";
import {JSONConfigsService} from "./jsonConfigs.service";
import {HttpModule, ResponseOptions, XHRBackend, Response} from "@angular/http";
import {ExampleService} from "./example.service";
import {MockBackend} from "@angular/http/testing";
import {ExampleDataMock} from "../util/NodeMockResponses.util";


function mockProviderArray():any[] {
    return [
        ExampleService,
        JSONConfigsService,
        {
            provide: XHRBackend,
            useClass: MockBackend
        }
    ];
}


describe('ExampleService', () => {
    beforeEach(()=>{
        TestBed.configureTestingModule({
           imports: [HttpModule],
           providers: mockProviderArray()
        });
    });


    describe('getExampleData$()', () => {
       it('Should return an observable with JSON object from API call as emitted value',
           async(inject([ExampleService, XHRBackend], (exSvc:ExampleService, mockBackend:MockBackend) => {
           mockBackend.connections.subscribe((connection:any) =>{
               connection.mockRespond(new Response(new ResponseOptions({body: JSON.stringify(ExampleDataMock)})))
           });

           exSvc.getExampleData$().subscribe((data)=>{
               expect(data).toEqual(ExampleDataMock);
           })
       })));
    });
});