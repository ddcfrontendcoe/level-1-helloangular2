import {Component, ElementRef, Renderer} from "@angular/core";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";
import {async, ComponentFixture, inject, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {ExamplePipe} from "./example.pipe";

@Component({
    selector: 'example-component',
    template: `<div><<p id='acctNumberParagraph' class='accountId-styles'>{{accountNumber | examplePipe}}</p></div>`,
})
export class MockExampleComponent{

    accountNumber: string;
    ngOnInit():void{
        this.accountNumber = "account3";
    }
}

function mockProviderArray():any[] {
    return [
    ];
}

describe ('test exampleDirective', () => {
    let comp: MockExampleComponent;
    let fixture: ComponentFixture<MockExampleComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserDynamicTestingModule
            ],
            declarations: [
                MockExampleComponent,
                ExamplePipe
            ],
            providers: mockProviderArray()
        }).compileComponents();

        fixture = TestBed.createComponent(MockExampleComponent);
        comp = fixture.componentInstance;
        fixture.autoDetectChanges();
    })

    it('check pipe transforms value',()=>{
        let de = fixture.debugElement.nativeElement;
        let pipeEl = de.querySelector('#acctNumberParagraph');
        expect(pipeEl.innerHTML).toEqual('Account3');
    });
});