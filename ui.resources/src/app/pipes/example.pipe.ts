import {Pipe, PipeTransform} from "@angular/core";

// Pipes take in input, transform it, and return it as output.
@Pipe({
    name: 'examplePipe'
})
export class ExamplePipe implements PipeTransform{

    // It is required to implement the 'transform" function when implementing PipTransform
    // This is basically what is called when the pipe runs on a value.
    transform(value:any, performAction: boolean){
        // Will capitalize the first letter of each word and return that value
        return value.charAt(0).toUpperCase() + value.slice(1);

    }
}