import {Component, DebugElement, ElementRef, Renderer} from "@angular/core";
import {async, ComponentFixture, inject, TestBed} from "@angular/core/testing";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";
import {ExampleDirective} from "./example.directive";
import {By} from "@angular/platform-browser";

@Component({
    selector: 'example-component',
    template: `<div><<p id='acctNumberParagraph' class='accountId-styles' [exampleDir]="accountNumber"> {{accountNumber}} </p></div>`,
})
export class MockExampleComponent{

    accountNumber: string;
    ngOnInit():void{
        this.accountNumber = "account3";
    }
}

function mockProviderArray():any[] {
    return [
        Renderer
    ];
}

describe ('test exampleDirective', () => {

    let comp: MockExampleComponent;
    let fixture: ComponentFixture<MockExampleComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserDynamicTestingModule
            ],
            declarations: [
                MockExampleComponent,
                ExampleDirective
            ],
            providers: mockProviderArray()
        }).compileComponents();

        fixture = TestBed.createComponent(MockExampleComponent);
        comp = fixture.componentInstance;
        fixture.autoDetectChanges();
    })

    it('check directive constructor', async(inject([Renderer], (rend: Renderer)=>{
        let eRef: ElementRef;
        let exampleDir: ExampleDirective = new ExampleDirective(rend, eRef);
        expect(exampleDir).toBeTruthy();
    })));

    it('check directive ', () => {
        let de = fixture.debugElement.query(By.css('#acctNumberParagraph'));
        expect(de.styles['font-size']).toEqual('30px');
    });


});
