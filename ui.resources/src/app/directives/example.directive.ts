import {Directive, ElementRef, Input, OnInit, Renderer} from "@angular/core";

@Directive({ selector: '[exampleDir]'})
export class ExampleDirective implements OnInit{
    // Name the input attribute the same as the directive just to make it easier to identify
    // You can have multiple inputs and none of them have to be the same name as the directive
    // A perk of using @Input instead of accessing the directive by its selector is that it allows you to use
    // data-bound variables, such as ones obtained from ngFor.
    @Input('exampleDir') exampleValue: string;

    constructor(private renderer: Renderer,
                private el: ElementRef,){
    }

    // ngOnInit is required when your component implements the lifecycle hook OnInit.
    // It is called after Angular first displays data-bound properties and after any input properties are set.
    ngOnInit(){
        if (this.exampleValue === "account3"){
            this.renderer.setElementStyle(this.el.nativeElement, 'font-size', '30px');
        }
    }
}