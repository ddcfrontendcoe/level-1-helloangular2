import {Component, OnInit} from "@angular/core";
import {templates} from "../../services/template.service";
import {ExampleService} from "../../services/example.service";
import {Observable} from "rxjs/Observable";
import {ExampleDirective} from "../../directives/example.directive";
import {ExamplePipe} from "../../pipes/example.pipe";

@Component({
    selector: "examplecomp",
    template: templates.GetTemplate("examplecomp.html")
})
export class ExampleComponent implements OnInit {

    // The convention for naming variables of the type observable or functions that return an observable should
    // end with a $.
    exampleObservable$: Observable<any>;

    // Service is made available for use through Dependency Injection.
    // This allows the services to run as Singleton.
    constructor(private exampleSvc: ExampleService) {
    }

    // ngOnInit is required when your component implements the lifecycle hook OnInit.
    // It is called after Angular first displays data-bound properties and after any input properties are set.
    ngOnInit(): void {
        console.log("ExampleComp");
        this.exampleObservable$ = this.exampleSvc.getExampleData$()
            .map((res: any) => {
                return res;
            });
    }
}