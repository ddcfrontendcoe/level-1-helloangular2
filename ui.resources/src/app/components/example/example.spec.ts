import {ComponentFixture, TestBed} from "@angular/core/testing";
import {ExampleComponent} from "./example.component";
import {async} from '@angular/core/testing';
import {ExampleService} from "../../services/example.service";
import {JSONConfigsService} from "../../services/jsonConfigs.service";
import {Observable} from "rxjs/Rx";
import {ExampleDataMock} from "../../util/NodeMockResponses.util";

function mockProviderArray():any[] {
    return [
        {
            provide: ExampleService,
            useValue:{
                getExampleData$ : () => {return Observable.of(ExampleDataMock)}
            }
        },
        {
            provide: JSONConfigsService
        }
    ];
}
describe('ExampleComponent test', () => {
    let component: ExampleComponent;
    let fixture: ComponentFixture<ExampleComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ExampleComponent ],
            providers: mockProviderArray()
        }).compileComponents();

        fixture = TestBed.createComponent(ExampleComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should exist', async(() => {
        expect(component).toBeTruthy();
    }));
});