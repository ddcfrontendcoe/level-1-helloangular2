import {AppComponent} from "./app.component";
import {NgModule} from "@angular/core";
import {ExampleComponent} from "./components/example/example.component";
import {ExampleService} from "./services/example.service";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {JSONConfigsService} from "./services/jsonConfigs.service";
import {ExampleDirective} from "./directives/example.directive";
import {ExamplePipe} from "./pipes/example.pipe";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule
    ],
    declarations: [
        //-- Components --//
        AppComponent,
        ExampleComponent,

        //-- Directives --//
        ExampleDirective,

        //-- Pipes --//
        ExamplePipe
    ],
    providers: [
        ExampleService,
        JSONConfigsService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}