// This is file to mock configuration.
// In some enterprise systems, they utilize a single configuration file in each environment to handle things like default api headers, api urls, and etc.
// This has the benefit of preventing you from having to hard code api urls within your application. If the team responsible for API's decide to
// change the path, or any other application configuration information, it can be done in a single file within the environment that does not require you to
// make any changes to your application. 

jsonConfigs = {
    feature: {
        "isMocked": true,
        "apiHeaders": {
            "X-appName": "HelloAngular2",
            "X-componentName": "Perficient Training",
            "X-apiKey": "some-api-key",
            "X-useragentcategory": "I",
            "X-sessionToken": false,
            "Content-Type": "application/json; charset\u003dUTF-8"
        }
    },
    global: {
        apiUrl: "http://localhost:9080",
        apipApiUrl: "http://localhost:9080",
        appName: "HelloAngular2",
        componentName: "Perficient Training",
        exampleUrl: "/exampleData/call"

    }
}
