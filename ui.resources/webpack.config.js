// webpack configuration file. Instructions for how your webpack should be built.

'use strict';

var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');

var path = require('path');
var bourbon = require('node-bourbon').includePaths;
var neat = require('node-neat').includePaths;
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackStrip = require('webpack-strip');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');

var stripConsoleLog = (process.env.STRIP_CONSOLE_LOG == 'true');

var loadersToUse = [
    {
        test: /\.ts$/,
        loader: 'ts-loader'
    },
    {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
                {
                    loader: 'css-loader' // translates CSS into CommonJS
                },
                {
                    loader: 'sass-loader', // compiles Sass to CSS
                    options: {
                        includePaths: [].concat(neat) // Loads Bourbon Neat
                    }
                }]
        })
    },
    {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'url-loader'
    },
    {
        test: /[^\s]*[^index]\.html/,
        use: 'file-loader?name=views/[name].[ext]'
    }
];

var pluginsToUse = [
    new HtmlWebpackPlugin({
        template: './src/index.html'
    }),
    new CopyWebpackPlugin([
        {from: 'test/data'}
    ]),
    new HtmlWebpackIncludeAssetsPlugin({
        assets: [
        'node_modules/lodash/lodash.js',
        'node_modules/ua-parser-js/src/ua-parser.js',
        ],
        append: false
    }),
    new ExtractTextPlugin('styles.bundle.css'),
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        warningsFilter: function() {return false;},
        compress: {
            // remove warnings
            // warnings: false,
            // Drop console statements
            // drop_console: stripConsoleLog
        },
    })
];

module.exports = {
    entry: {
        app: ['./jsonConfigs.mock.js', './src/main.ts', './src/main.scss']
    },
    devtool: "#inline-source-map",
    stats: {warnings: false},
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist/mcc')
    },
    module: {
        loaders: loadersToUse,
        exprContextCritical: false
    },
    resolve: {
        modules: ['node_modules', 'node_modules/styleguide', __dirname + '/src'],
        extensions: ['.js', '.ts', '.scss']
    },

    plugins: pluginsToUse

};
