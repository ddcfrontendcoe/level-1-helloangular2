var exampleData = require("./exampleData/exampleData.json");

// These files are used to essentially catch API calls that are made along with the help of dyson.
// You can also watch for other specific qualities such as query parameters.
// This is useful because it allows you to write your code using the API's that exist in production and simulate
// their response.
module.exports = {
    cache: false,
    path: '/exampleData/call',
    template: function(params, query, body, cookie, headers) {
        return exampleData;
    }
}