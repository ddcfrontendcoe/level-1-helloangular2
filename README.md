# Assignment Title: HelloAngular2
# Assignment Level: Entry (Level 1)

## What is Angular 2? 

Angular 2 is a platform used to build client-side web applications using TypeScript / JavaScript. Angular 2 utilize's data binding, html templating, a component architecture, and many other features that facillate good application design. 

For binding Angular 2 supports data, property, event, and two-way data binding. This is a powerful feature that when combined with the Observer pattern allows for your web page to be truly reactive to data. The observer pattern can be 
implemented with the help of libraries such as RXjs.

Observer Pattern Documentation from creators of RXjs: http://reactivex.io/documentation/observable.html
RXjs Documentation: http://reactivex.io/rxjs/

Inside of this assignment is an example of data binding as well as an implementation of an Observable. 

Some more of the tools implemented by Angular 2 include components, pipes, and directives. 

Components in Angular 2 allow you to easily seperate different features of your webpage. Each component in angular two has its own JavaScript and html template. 

Directives in Angular 2 are essentialy a way for you to define a custom HTML tag attribute. 

Pipes in Angular 2 are intended to serve the purpose of a single function. Pipes are used directly in your HTML, and while it mamy not always be optimal to use them. It is good to know about them and future assignments
will go into more detail about when it is good and bad to use pipes. 

Inside of this assignment are a few examples of each one of these features. 


## Project Description
Below is a description of this project and its purpose. See Assignment instructions for details
about getting started. 

This project is intended to be used as a starting point for new developers learning
Angular 2. The structure of this application will be outlined below. Comments will be placed in each file giving a brief overview of what it does and how it works. 
### misc/
This directory is used to hold some of the files needed for the assignment.

###ui.resources/ 
Directory is used to store all Angular related files. Typically needed in bigger projects. 

###ui.resources/src/
This directory is where the javascript, typescript, scss, and html files are stored for the application

### ui.resources/src/app/
Contains all the components, directives, pipes, service files, etc.  

### ui.resources/src/app/app.component.ts 
Considered the root component of your angular app. Basically, this is the first component that is loaded
  
### ui.resources/src/app/app.module.ts  
This describes the parts needed for the application and how those parts fit together.

### ui.resources/src/index.html
index file that is displayed in your browser. In this application, this is also where all the templates are stored for your components.

### ui.resources/src/main.scss
This is the main scss file. It is used as a way to load in all your SCSS files in one place.
 
### ui.resources/src/main.ts 
Used to bootstrap the starting point of the application. 

### ui.resources/test/
This directory stores mock data used by dyson. This allows you to mock api calls.

### ui.resources/test/data/mock/get/
This is where all the mock data is setup and stored for get requests. 

### ui.resources/.gitignore: 
Used to tell git to ignore certain files. For this application we want to ignore the node_modules directory
and the npm debug log

### ui.resources/base.spec.ts:
Similar to the main.ts file but for unit tests. It is where the unit tests beging to run.
 
### ui.resources/jsonConfigs.mock.js:
In some enterprise applications they will have a configuration file hosted somewhere that 

### ui.resources/karma.conf.js:
Configuration file used when running karma

### ui.resources/package.json:
Used to list necessary npm packages, package information, and define scripts for reuse.

### ui.resources/tsconfig.json:
Typescript configurations

### ui.resources/webpack.config.js:
Configuration file for webpack

## Assignment Instructions
The goal of this assignment is to become introduce yourself to some of the features of angular.
You will create a component that utilizes a service with some custom scss, a pipe to transform some of the data displayed,
and a directive to edit the html displayed. Each one of these files will also need to be unit tested.

If you get stuck at any point feel free to reference the example files or contact Kaleb Heinzen for assistance.

The first thing that needs to be done is an "npm install". This command needs to be run where package.json is stored.
So you will need to navigate inside the ui.resources directory from command line and run npm install. Most IDE's have a terminal window you can open up
directly inside of them.

Visual Studio Code: 
 1) Click View 
 2) Click Integrated Terminal
 
WebStorm:
 1) Click View
 2) Click Tool Windows
 3) Click Terminal
 
This will bring in all the node packages listed in package.json. This requires npm to be installed on your system. Instructions to do so can be
found here: https://docs.npmjs.com/getting-started/installing-node

In package.json there is a section labeled "scripts". These are used as shortcuts to execute different command line instructions. Instructions for running these commands:
 1) Open a terminal window
 2) Navigate to directory that has your package.json file
 3) Type npm your-command-name then hit enter and it should start running
 
Webstorm offers a way to quickly run all of these scripts:
 1) Find your package.json in your project browser
 2) Right click package.json
 3) Click Show npm Scripts
 4) Double click any of the scripts to run them

1) Mock out a response made to the API URI: '/banking/members/'. The response should be formated as found in the misc/members.json file. 
For guidance, check the files found in ui.resources/test/data/mock/get/.
 
2) Write a service file that makes a call to the '/banking/members' API and returns this information as an observable. 
For guidance, look in ui.resources/src/app/services/example.service.ts

3) Create a component that displays the members names and account balances. 
For guidance, look in ui.resources/src/app/components/example and ui.resources/src/app/index.html

4) Add some custom styles to your component inside its own scss file and import that into main.scss.
 
5) Write a function inside the component to capitalize the name of each member
(hint: you can call this function either in the html or inside the component.ts file)

6) Write a directive that highlights account balances over 1000 in green.
 
7) Write a pipe that converts account balances to be in currency format (1000 should be $1000.00)

## How to serve application on localhost
To host the application locally run these two commands found in package.json
  
"serve": "webpack-dev-server --inline --progress --config webpack.config.js"  
"dyson": "dyson test/data/mock 9080"  

"serve" builds the webpack according to the webpack.config.js file, and hosts it. This is done through 
the webpack-dev-server npm module.

"dyson" spins up and listens for any API calls and responds accordingly. 

Both "serve" and "dyson" need to be running  at the same time. 

## How to serve application on network
1) Run 'ipconfig' ('ifconfig' for Mac) from command line to obtain the ip address of your computer. 
2) Find the "serve" command located in package.json and add --host YOUR_IP_HERE to the end of the command.
3) Go to 'node_modules/dyson/lib/dyson.js' and edit the line 'server=app.listen(options.port)' to include your ip address. So it will become 'server = app.listen(options.port, "YOUR_IP_HERE");'
4) Edit 'jsonConfigs.mock.js' by changing the 'apiUrl: "http://localhost:9080",' line to 'apiUrl: "http://YOUR_IP_HERE:9080",' and the 'apipApiUrl: "http://localhost:9080",' line to 'apipApiUrl: "http://YOUR_IP_HERE:9080",'
5) You should now be able to access the page on any device on your network by navigating to 'http://YOUR_IP_HERE:8080/'.

## How to run unit tests
To run unit tests you will need to use the following command from package.json  

"test": "karma start karma.conf.js --single-run"

This starts up karma according to the karma.conf.js. 